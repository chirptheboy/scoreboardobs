# ScoreboardOBS
Tiny webpage scoreboard for use in an OBS browser plugin.

## Preview
![alt text](src/preview.png "Sample")

## Use
1. Download the project zip and extract locally
2. Add a Browser element in OBS and point to scoreboard.html
3. Resize as needed so only the actual scoreboard is visible
4. Open scoreboard.html in a separate browser to control the page on the fly

## Notes
* Click the 'Debug' checkbox to get an alert of variable values
